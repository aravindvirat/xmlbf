{ mkDerivation, aeson, base, bytestring, containers, deepseq, QuickCheck
, quickcheck-instances, selective, stdenv, tasty, tasty-hunit
, tasty-quickcheck, text, transformers, unordered-containers
}:
mkDerivation {
  pname = "xmlbf";
  version = "0.6.1";
  src = ./.;
  libraryHaskellDepends = [
    aeson base bytestring containers deepseq selective text transformers
    unordered-containers
  ];
  testHaskellDepends = [
    aeson base bytestring QuickCheck quickcheck-instances tasty tasty-hunit
    tasty-quickcheck text transformers
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "XML back and forth! Parser, renderer, ToXml, FromXml, fixpoints";
  license = stdenv.lib.licenses.asl20;
}
