builtins.fetchTarball {
  url = "https://github.com/NixOS/nixpkgs-channels/archive/3d6cdfa24bd771b36e8c87241d17275dd144c7a3.tar.gz";
  sha256 = "1cif0z3zwwr2m5ibrm2b51dw8pqfqv4n95xhfiqr8zl941k18gkr";
}
